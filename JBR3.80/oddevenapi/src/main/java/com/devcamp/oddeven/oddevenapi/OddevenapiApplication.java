package com.devcamp.oddeven.oddevenapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OddevenapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OddevenapiApplication.class, args);
	}

}
