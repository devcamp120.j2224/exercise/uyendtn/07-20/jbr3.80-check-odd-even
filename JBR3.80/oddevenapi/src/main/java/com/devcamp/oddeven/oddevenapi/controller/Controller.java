package com.devcamp.oddeven.oddevenapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("checknumber")
    public String checkNumber() {
        int number = 15;
        if (number%2 != 0){
            return "This number is odd number!";
        } else {
            return "This number is even number!";
        }
    }
}
